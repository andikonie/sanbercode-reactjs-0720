// soal no 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var spasi =" ";
var kataKeduaCapitalize = kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1);

var hasil = kataPertama.concat(spasi,kataKeduaCapitalize,spasi,kataKetiga,spasi,kataKeempat.toUpperCase());
console.log("\n");
console.log(hasil);

//================= soal no 2 ================= 
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var angkaPertama = parseInt(kataPertama);
var angkaKedua = parseInt(kataKedua);
var angkaKetiga = parseInt(kataKetiga);
var angkaKeempat = parseInt(kataKeempat);
var hasil = angkaPertama+angkaKedua+angkaKetiga+angkaKeempat

console.log("\n");
console.log("total semua : "+hasil);

//================= soal no 3 =================
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua =kalimat.substring(3,14);
var kataKetiga =kalimat.substring(15,18); // do your own! 
var kataKeempat =kalimat.substring(19,24); // do your own! 
var kataKelima =kalimat.substring(25,31); // do your own! 

console.log("\n");
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//soal no 4
var nilai=50;
var index;
    if(nilai>=80)
    index ='A';
    else if(nilai>=70 && nilai <80)
    index='B';
    else if(nilai>=60 && nilai <70)
    index='C';
    else if(nilai>=50 && nilai <60)
    index='D';
    else
    index = 'E';
    console.log("\n");
console.log("nilai : "+nilai+" maka index: "+index);

//================= soal no 5 ==============================
var tanggal = 25;
var bulan = 8;
var tahun = 1995;
var bulanKarakter;
var ttl;

switch(bulan){
    case 1: bulanKarakter='Januari'; break;
    case 2: bulanKarakter='Februari'; break;
    case 3: bulanKarakter='Maret'; break;
    case 4: bulanKarakter='April'; break;
    case 5: bulanKarakter='Mei'; break;
    case 6: bulanKarakter='Juni'; break;
    case 7: bulanKarakter='Juli'; break;
    case 8: bulanKarakter='Agustus'; break;
    case 9: bulanKarakter='September'; break;
    case 10: bulanKarakter='Oktober'; break;
    case 11: bulanKarakter='November'; break;
    case 12: bulanKarakter='Desember'; break;
    default: bulanKarakter="Bukan bulan"; break;
}
console.log("\n");
console.log(tanggal.toString()+" "+bulanKarakter+" "+tahun.toString());