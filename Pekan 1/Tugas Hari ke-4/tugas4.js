//soal 1

console.log("Soal 1");
var i=2;
console.log("Looping Pertama ");
while(i<=20){
    console.log(i + " - I Love Coding");
    i +=2;
}
console.log("Looping Kedua ");
while(i>2){
    i -=2;
    console.log(i + " - I Will become FrontEnd Developer");
}
console.log(" \n");
//soal 2
console.log("Soal 2");
var x =1;

for(x;x<20;x++){
    //console.log(x%3);
    if(x%3 == 0 && x%2 == 1){
        console.log(x+"- I Love Coding");
    }
    else if(x%2 == 1){
        console.log(x+" - Santai");
    }
    else if(x%2 == 0){
        console.log(x+" - Berkualitas");
    }
}
console.log(" \n");
//soal 3
console.log("Soal 3");
var i;
var j;
var cetak="";
for(i=0;i<7;i++){
    for(j=0;j<=i;j++){
        cetak +="#";
        //console.log(i , j);
    }
    cetak+="\n";
}
console.log(cetak);
console.log(" \n");
//soal 4
console.log("Soal 4");
var kalimat="saya sangat senang belajar javascript"
var kalimatJadi = [];
var kalimatArray = kalimat.split(" ");
for(var i=0;i<kalimatArray.length;i++){
    kalimatJadi.push("\"" +  kalimatArray[i]+ "\"") ;
}
console.log(kalimatArray);

console.log(kalimatJadi);
console.log(" \n");

//soal 5
console.log("Soal 5");
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var buahSort = daftarBuah.sort();
for(var i=0;i < daftarBuah.length;i++){
    console.log(buahSort[i]);
}