//soal no 1
console.log("soal no 1 ");
var pesan
function halo(){
    pesan = "Halo Sanbers!";
    return pesan;
}
 
console.log(halo()) // "Halo Sanbers!" 
console.log();


//soal no 2
console.log("soal no 2 ");
 
var num1 = 12
var num2 = 4

function kalikan(a,b){
    return a*b;
}
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log();

//soal no 3
console.log("soal no 3 ");
/* 
    Tulis kode function di sini
*/
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
function introduce(a,b,c,d){
    var kata = "Nama saya "+a+" ,umur saya "+b+" tahun, alamat saya di "+c+", dan saya punya hobby yaitu "+d+"!";
    return kata;
}
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 