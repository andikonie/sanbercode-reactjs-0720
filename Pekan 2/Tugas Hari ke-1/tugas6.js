// soal no.1
console.log("Soal no. 1");
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];

var orang = {
  nama: "Asep",
  kelamin: "laki-laki",
  hobby: "baca buku",
  tahunLahir: 1992,
};

console.log(orang);
console.log();

// soal no 2
console.log("Soal no. 2");
var buah = [
  {
    nama: "Strawberry",
    warna: "Merah",
    adaBiji: "tidak",
    harga: 9000,
  },
  {
    nama: "jeruk",
    warna: "oranye",
    adaBiji: "ada",
    harga: 8000,
  },
  {
    nama: "Semangka",
    warna: "Merah",
    adaBiji: "ada",
    harga: 10000,
  },
  {
    nama: "Semangka",
    warna: "Hijau",
    adaBiji: "ada",
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    adaBiji: "tidak",
    harga: 5000,
  },
];

console.log(buah[0]);
console.log();

//soal no 3
console.log("Soal no. 3");
var dataFilm = [];
function addFilm(nama, durasi, genre, tahun) {
  dataFilm.push({ nama: nama, durasi: durasi, genre: genre, tahun: tahun });
}
addFilm("Braveheart", 180, "Action", 1994);
console.log(dataFilm);
console.log();

//soal 4
console.log("soal no. 4");
class Animal {
  constructor(name) {
    this.name = name;
    this.kaki = 4;
    this.darah_dingin = false;
  }
  get legs() {
    return this.kaki;
  }

  get cold_blooded() {
    return this.darah_dingin;
  }
}

var sheep = new Animal("shaun");
console.log("release 1");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

// Code class Ape dan class Frog di sini
class Ape extends Animal {
  constructor(name) {
    super(name);
    this.kaki = 2;
  }
  yell() {
    return console.log("Auoooo");
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
    this.kaki = 2;
  }
  jump() {
    return console.log("hop hop");
  }
}
console.log();
console.log("release 2");
var sungokong = new Ape("Kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

//soal no 5
console.log();
console.log("Soal No 5");
class Clock {
  constructor({ template }) {
    var timer;

    function render() {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = "0" + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = "0" + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = "0" + secs;

      var output = template
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs);

      console.log(output);
    }

    this.stop = function () {
      clearInterval(timer);
    };

    this.start = function () {
      render();
      timer = setInterval(render, 1000);
    };
  }
}
var clock = new Clock({ template: "h:m:s" });
clock.start();
