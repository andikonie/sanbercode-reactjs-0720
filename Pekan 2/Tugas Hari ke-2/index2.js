var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function bacaBukuPromise(waktu,index) {
    readBooksPromise(waktu,books[index])
        .then(function(fulfilled) {
            index++;
            
            if(waktu>fulfilled && index<books.length){
            waktu=fulfilled;
            
            bacaBukuPromise(waktu,index)
            }
        })
        .catch(function(error) {
            console.log("waktu habis");
        });
}
 

bacaBukuPromise(10000,0); 