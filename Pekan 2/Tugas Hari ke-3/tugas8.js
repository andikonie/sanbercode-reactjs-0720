//soal no 1
console.log("SOAL no 1 :")
let luasLingkaran = (r) => {
    const pi = 22/7;
    return pi * (r*r);
}

console.log(luasLingkaran(7));

let kelilingLingkaran =(r) => {
    const pi = 22/7;
    return pi * (2*r);
}
console.log(kelilingLingkaran(7));

let kalimat = "";
const spasi = " ";
console.log();

//soal no 2\
console.log("SOAL no 2 :")
let tambahKalimat = ( kata ) => {
    kalimat = kalimat.concat(`${kata}`);
    kalimat = kalimat.concat(`${spasi}`);
}
tambahKalimat("saya");
tambahKalimat("adalah");
tambahKalimat("seorang");
tambahKalimat("frontend");
tambahKalimat("developer");
console.log(kalimat);
console.log();


//soal no 3
console.log("Soal no 3")
class Book{
    constructor(name, totalPage, price){
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }

}

class Komik extends Book{
    constructor(name,totalPage,price, isColorful){
        super(name,totalPage,price);
        
        this.isColorful = isColorful;
    }
}

let koding = new Komik("Coding",200,150000,true);
console.log(koding)