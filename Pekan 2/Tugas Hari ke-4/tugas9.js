//soal no 1
console.log("SOAL NO 1 =====================");

const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 
console.log();

//soal no 2
console.log("SOAL NO 2 =====================");

  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const {firstName, lastName, destination, occupation, spell} = newObject;
  console.log(firstName, lastName, destination, occupation)
console.log();


//soal no 3
console.log("SOAL NO 3 =====================");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined =[...west, ...east];
//Driver Code
console.log(combined)
